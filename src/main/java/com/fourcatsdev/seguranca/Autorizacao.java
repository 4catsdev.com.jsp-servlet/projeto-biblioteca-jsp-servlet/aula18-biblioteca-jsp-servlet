package com.fourcatsdev.seguranca;


public class Autorizacao {
	// aula 18
	public String indexPerfil(DetalheUsuario detalheUsuario) {
		String redirectURL = "";
		if (verificarPerfil(detalheUsuario, "ADMIN")) {
			redirectURL = "auth/admin/admin-index.jsp";
		} else if (verificarPerfil(detalheUsuario, "BIBLIO")) {
			redirectURL = "auth/biblio/biblio-index.jsp";
		} else if (verificarPerfil(detalheUsuario, "USER")) {
			redirectURL = "auth/user/user-index.jsp";
		}
		return redirectURL;
	}	
	// aula 18
	private boolean verificarPerfil(DetalheUsuario detalheUsuario, String papel) {
		boolean temPerfil = detalheUsuario.getPapeis().contains(papel);
		return temPerfil;
	}
}
